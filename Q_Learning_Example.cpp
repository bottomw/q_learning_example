// Q_Learning_Example.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

using namespace std;

const int qSize = 6;
const double gamma = 0.8;
const int iterations = 2;
int initialState[qSize] = {1, 3, 5, 2, 4, 0};
int Q[qSize][qSize];

int R[qSize][qSize] = {{-1, -1, -1, -1, 0, -1},
			           {-1, -1, -1, 0, -1, 100},
			           {-1, -1, -1, 0, -1, -1},
			           {-1, 0, 0, -1, 0, -1},
			           {0, -1, -1, 0, -1, 100},
			           {-1, 0, -1, -1, 0, 100}};

int currentState;

void initialize()
{
	srand((unsigned)time(0));
	for(int i=0;i<=(qSize-1);i++)
	{
		for(int j=0;j<=(qSize-1);j++)
		{
			Q[i][j] = 0;
		}
	}
};

int get_random_action(int upperBound, int lowerBound)
{
	int rdmAction;
	bool choiceValid = false;
	int range = (upperBound - lowerBound);
	do 
	{
		rdmAction = lowerBound + int(range * rand()/(RAND_MAX+1.0));
		if(R[currentState][rdmAction] > -1)
		{
			choiceValid = true;
		}
	}while(choiceValid == false);
	return rdmAction;
};

int maximum(int state, bool returnIndexOnly)
{
	int maxValueOpt = 0;
	bool done = false;
	do
	{
		bool foundNewWinner = false;
		for(int i = 0; i <= (qSize - 1); i++)
		{
			if(i == maxValueOpt){continue;};
			if(Q[state][i] > Q[state][maxValueOpt])
			{
				maxValueOpt = i;
				foundNewWinner = true;
			}
		}
		if(foundNewWinner == false){done = true;};
	}while(done = false);
	if(returnIndexOnly == true)
	{
		return maxValueOpt;
	}
	else
	{
		return Q[state][maxValueOpt];
	}
}


int reward(int action)
{
	return static_cast<int>(R[currentState][action] + (gamma * maximum(action, false)));
};

void print_state()
{
	cout<<"current state: "<<currentState<<endl;
	for(int i=0;i<=(qSize-1);i++)
	{
		for(int j=0;j<=(qSize-1);j++)
		{
			cout<<setw(5)<<Q[i][j];
			if(j<(qSize-1))
			{
				cout<<',';
			};
		};
		cout<<"\n";
	}
	cout<<'\n';
}

void choose_an_action()
{
	int possibleAction;
	possibleAction=get_random_action(qSize, 0);
	cout<<"random possible action: "<<possibleAction<<endl;
	if(R[currentState][possibleAction]>=0)
	{
		Q[currentState][possibleAction] = reward(possibleAction);
		print_state();
		currentState = possibleAction;
	}
};

void episode(int initialStateEpisode)
{
	currentState = initialStateEpisode;
	do
	{
		choose_an_action();
	} while(currentState != 5);
	for(int i=0; i<=(qSize-1); i++)
	{
		choose_an_action();
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	int newState;
	initialize();
	for(int iter=0;iter<=(iterations-1);iter++)
	{
		for(int i=0;i<=(qSize-1);i++)
		{
			cout<<"iterations: "<<iter<<endl;
			cout<<"initial state: "<<initialState[i]<<endl;
			episode(initialState[i]);
			//print_state();
		}
	}
	//print out matrix
	for(int i=0;i<=(qSize-1);i++)
	{
		for(int j=0;j<=(qSize-1);j++)
		{
			cout<<setw(5)<<Q[i][j];
			if(j<(qSize-1))
			{
				cout<<',';
			};
		};
		cout<<"\n";
	}
	cout<<'\n';
	//agent walk according to Q matrix
	for(int i=0;i<=(qSize-1);i++)
	{
		currentState = initialState[i];
		newState = 0;
		do
		{
			newState=maximum(currentState, true);
			cout<<currentState<<",";
			currentState=newState;
		}while(currentState!=5);
		cout<<"5"<<endl;
	};
	system("pause");
	return 0;
};

